class Almacenamiento {
    static agregar() {
        localStorage.setItem("stock", JSON.stringify(BaseDeDatos.misDatos()));
    }

    static borrar() {

    }

    static consultar() {
        let miStock = JSON.parse(localStorage.getItem("stock"));
        console.log(miStock.nombre);

        miStock = miStock.map(function (unElemento) {
            console.log(unElemento.marca);
        });
    }
}


class Proveedores {
    static agregar(){
        let miStock = JSON.parse(localStorage.getItem("stock"));
        let miProducto = miStock.find( unProductos => unProductos.marca === "Rivadavia");                
        //Voy a querer agregar un objeto que me pasa otro metodo
        //Este es el objeto que vendria de otro metodo:
        let objetoParaAgregar = {
                        nombre : "Nuevo proveedor",
                        direccion : "Nueva calle al 7000"
                    };
        miProducto.proveedores.push(objetoParaAgregar);                  
        console.log(miProducto.proveedores);
        
        //Agregar nuevo objeto al array y luego ponerlo en el localStorage

        //localStorage.setItem("",);                        
    }
    
    static agregarRepaso(){
        //Agrgegar un nuevo proveedor al listado
        let miStock = JSON.parse( localStorage.getItem("stock"));
        console.log(miStock);
        let productoEncontrado = miStock.find( unProducto => unProducto.marca === "Rivadavia");
        console.log("Eccontrado: " + JSON.stringify(productoEncontrado));
        let  proveedorProvisorio = {
            nombre : "Roberto",
            direccion : "Peru al 700"
        };
        
        productoEncontrado.proveedores.push(proveedorProvisorio);
        console.log("Producto encontrado: " + productoEncontrado);
        console.log("Listado completo: " +  JSON.stringify(miStock));
        
        localStorage.setItem("stock",JSON.stringify(miStock));
    }
    
    static quieroProbarConFilter() {
        let miStock = JSON.parse( localStorage.getItem("stock"));
        let miResultado = miStock.filter(unProducto => unProducto.marca === "Rivadavia");
        console.log(miResultado);
    }
    
    
    static consultar(){
        let miStock = JSON.parse(localStorage.getItem("stock"));
        let miProducto = miStock.find( unProductos => unProductos.marca === "Rivadavia");
        console.log("miProducto: " + JSON.stringify( miProducto));
        console.log("proveedores: " + JSON.stringify( miProducto.proveedores));
    }
}

class BaseDeDatos {
    static misDatos() {
        let stock = [
            {
                cantidad: 1000,
                nombre: "cuadernos",
                marca: "Gloria",
                proveedores : [
                    {
                        nombre : "Pepe",
                        direccion : "Cucha cucha 900"
                    },
                    {
                        nombre : "Jose",
                        direccion : "Independecnia 800"
                    },
                    {
                        nombre : "Alberto",
                        direccion : "Peru 820"
                    }
                ]
            },
            {
                cantidad: 9000,
                nombre: "cuadernos",
                marca: "Rivadavia",
                proveedores : [
                    {
                        nombre : "Juan",
                        direccion : "la plata 200"
                    },
                    {
                        nombre : "Maria",
                        direccion : "Bolivar 100"
                    },
                    {
                        nombre : "Sebastian",
                        direccion : "Rivadavia 500"
                    }
                ]
            },
            {
                cantidad: 789,
                nombre: "cuadernos",
                marca: "Exito"
            },
            {
                cantidad: 666,
                nombre: "cuadernos",
                marca: "Rivadavia"
            },
            {
                cantidad: 888,
                nombre: "cuadernos",
                marca: "Rivadavia"
            }
        ];
        return stock;
    }
}

Almacenamiento.agregar();